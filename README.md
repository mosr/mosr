# MOSR

MOSR's aim is to be:
* a multipurpose supervision software.
* scalable
* easy to use
* what else ?

> At this stage, it is a path-finder, even if I use it personnaly in production.<br>
> Behaviour, Configuration, protocols may completly change without notice.

## Installation

```bash
pip install git+https://gitlab.com/mosr/mosr.git#master
```

## Config

### Config sources
The first file found in `$MOSR_CONFIG`, `~/.config/mosr.conf`, `/etc/mosr.conf` will be used.

Content:

```yaml
db_file: /var/lib/mosr/mosr.db
```

A config parameter can also be retrieved from env:

```bash
MOSR_CONFIG_DB_FILE=path/to/mosr.db mosr_show some.resource green
```

### Config parameters

#### DB File

|   Name  | Env | Description | Default |
|---------|-----|-------------|---------|
| db_file | MOSR_CONFIG_DB_FILE | Path to database file | None (Mandatory) |
| slack_webhook | MOSR_CONFIG_SLACK_WEBHOOK | Url of slack webhook  | None (Mandatory) |
| server_bind | MOSR_CONFIG_SERVER_BIND | Server listen interface | 0.0.0.0 (All) |
| server_port | MOSR_CONFIG_SERVER_PORT | Server listen port | 8080 |
| server_pass | MOSR_CONFIG_SERVER_PASS | Server password | None (Mandatory) |
| develop | MOSR_CONFIG_DEVELOP | enable developement helpers | False |
 
## Usage

### `mosr_push resource.name [red|green]`

> Push a resource status

### `mosr_show`

> Show resources status

### `mosr_service`

> Run app service (webservice, scheduled tasks)

#### HTTP client usage example

>  curl -sv -u mosr:password -X PUT http://127.0.0.1:8080/status/resource.name/green

## Developement

### Dependencies

```
python3 -m venv .venv
source .venv/bin/activate
pip install -U 'pip>=21.0.0' 'wheel>=0.37.0'
pip install -r requirements-dev.txt -r requirements.txt
```

### Actions

* Format Code: `isort src/ ; black src/`
* Lint: `prospector src/`
* Launch tests: `python3 -m unittest discover -v -p '*.py' -s ./src`

## License (AGPL)

```
Copyright (C) 2021  Guillaume ZITTA
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.
You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```