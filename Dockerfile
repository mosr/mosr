# syntax = docker/dockerfile:1.3-labs

FROM python:3.9

RUN --mount=target=/tmp/app,rw --mount=type=cache,target=/root/.cache/pip <<EOSCRIPT
pip install --use-feature=in-tree-build /tmp/app
cp /tmp/app/docker/entrypoint.sh /
EOSCRIPT

ENTRYPOINT [ "/entrypoint.sh" ]