#!/bin/bash

if ! [[ "$*" == "" ]]; then
    exec "$@"
fi

exec mosr_service