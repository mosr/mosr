import os
import sqlite3
from typing import TYPE_CHECKING

from .config import config
from .resource import Resource

if TYPE_CHECKING:
    from typing import Iterator, Optional


class Database:
    _connexion: sqlite3.Connection
    _cursor: sqlite3.Cursor

    def __init__(self) -> None:
        filename = config.db_file
        new_db = not os.path.exists(filename)

        # Ensure parent dir exists
        parent_dir = os.path.dirname(filename)
        if not os.path.exists(parent_dir):
            os.makedirs(parent_dir)

        self._connexion = sqlite3.connect(filename)
        self._cursor = self._connexion.cursor()

        if new_db:
            self._cursor.execute(
                """
                create table resource (
                    name TEXT CONSTRAINT resource_pk PRIMARY KEY,
                    status TEXT,
                    last_update INTEGER
                );
                """
            )

    def update(self, resource: "Resource"):
        self._cursor.execute(
            """insert into resource (name, status, last_update) values(:name,:status,:last_update)
               on conflict(name) do update set status=:status, last_update=:last_update;
            """,
            {
                "name": resource.name,
                "status": resource.status,
                "last_update": resource.last_update,
            },
        )
        self._connexion.commit()

    def list(self) -> "Iterator[Resource]":
        self._cursor.execute("select name, status, last_update from resource order by name")
        row = self._cursor.fetchone()
        while row is not None:
            yield Resource(*row)
            row = self._cursor.fetchone()

    def get(self, name: str) -> "Optional[Resource]":
        self._cursor.execute("select name, status, last_update from resource where name=:name", {"name": name})
        row = self._cursor.fetchone()
        if row is not None:
            return Resource(*row)

        return None

    def delete(self, name: str) -> "None":
        print(name)
        self._cursor.execute("delete from resource where name=:name", {"name": name})
        self._connexion.commit()

    def sql_execute(self, *args, **kwargs):
        self._cursor.execute(*args, **kwargs)
        self._connexion.commit()
