import json


class Resource:
    name: str
    status: str
    last_update: str

    def __init__(self, name: str, status: str, last_update: str) -> None:
        self.name = name
        self.status = status
        self.last_update = last_update

    def __str__(self) -> str:
        return json.dumps(self)
