from typing import TYPE_CHECKING

from slack_sdk.webhook import WebhookClient

from mosr.config import config

if TYPE_CHECKING:
    from typing import List

    from mosr.resource import Resource


class Notification:
    def __init__(self) -> None:
        self._webhook = WebhookClient(config.slack_webhook)

    def send(self, resources: "List[Resource]"):
        text = """# MOSR Status

"""

        for resource in resources:
            text += f"* :large_{resource.status}_circle: {resource.name}\n"

        block = {
            "type": "section",
            "text": {"text": text, "type": "mrkdwn"},
        }

        try:
            response = self._webhook.send(blocks=[block])
        except Exception as e:
            raise Exception("Notificaiton failed, {e}") from e

        if response.status_code != 200:
            raise Exception(f"Bad slack response, code: {response.status_code}, message: {response.body}")
