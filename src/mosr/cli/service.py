import uvicorn
from apscheduler.schedulers.background import BackgroundScheduler

from mosr.config import config
from mosr.webservice.cron import run as run_cron


def main():
    ### Initiate scheduler
    # UTC or whatever is good for interval based scheduling.
    scheduler = BackgroundScheduler(timezone="UTC")
    # Run once then plan interval
    scheduler.add_job(run_cron)
    scheduler.add_job(run_cron, "interval", seconds=30 * 60)
    scheduler.start()

    ### Web worker
    # Enable reload on code change when developping
    reload = config.develop
    uvicorn.run(
        "mosr.webservice.main:app", host=config.server_bind, port=config.server_port, log_level="info", reload=reload
    )
