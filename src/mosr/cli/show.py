from click import style
from columnar import columnar

from mosr.resources import Resources

HEADERS = ["status", "name", "last_update"]
PATTERNS = [
    ("green", lambda text: style(text, fg="green")),
    ("red", lambda text: style(text, fg="red")),
    ("purple", lambda text: style(text, fg="magenta")),
]


def main():
    table_list = [[r.status, r.name, r.last_update] for r in Resources().list()]
    table = columnar(table_list, HEADERS, no_borders=True, patterns=PATTERNS)
    print(table)
