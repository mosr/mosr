import sys

from mosr.resources import Resources


def main():
    (name, status) = (sys.argv[1], sys.argv[2])
    Resources().push(name, status)
