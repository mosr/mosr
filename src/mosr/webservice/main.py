import secrets

from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.security import HTTPBasic, HTTPBasicCredentials

from mosr.config import config
from mosr.resources import Resources

app = FastAPI()
security = HTTPBasic()


def check_creds(credentials: HTTPBasicCredentials = Depends(security)):
    correct_username = secrets.compare_digest(credentials.username, "mosr")
    correct_password = secrets.compare_digest(credentials.password, config.server_pass)
    if not (correct_username and correct_password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect email or password",
            headers={"WWW-Authenticate": "Basic"},
        )
    return credentials.username


@app.put("/status/{name}/{status}", dependencies=[Depends(check_creds)])
async def push(name: str, status: str):
    Resources().push(name, status)
    # IDEA: can status be re-evaluated from server ?
    #   if a resource is redundant it may be useful to inform
    #   of the state of general status

    return {"status": "OK"}
