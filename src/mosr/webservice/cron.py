from mosr.notification import Notification
from mosr.resources import Resources


def run():
    resources = Resources()
    resources.update_nonews()

    resources_to_notify = []

    for resource in resources.list():
        if resource.status != "green":
            resources_to_notify.append(resource)

    if len(resources_to_notify) > 0:
        Notification().send(resources_to_notify)


if __name__ == "__main__":
    # Useful for VSCode
    run()
