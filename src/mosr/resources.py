from datetime import datetime

from mosr.db import Database
from mosr.resource import Resource


class Resources(Database):
    def push(self, name: str, status: str):
        resource = Resource(name, status, datetime.now())
        self.update(resource)

    def update_nonews(self):
        self.sql_execute(
            """
            update resource set status='purple' where last_update < datetime('now', 'localtime', '-30 minutes');
        """
        )
