"""
Configuration
"""
import os
from typing import Optional

import yaml

CONFIG_FILE_CANDIDATES = [
    os.environ.get("MOSR_CONFIG"),
    os.path.expanduser("~/.config/mosr.conf"),
    "/etc/mosr.conf",
]


class _Config:
    """Configuration parameters."""

    _data = None

    def __init__(self) -> None:
        if self._data is None:
            self._data = self._get_data()

    def _get_data(self):
        """Try to load data from config files.

        It iterates over config files candidates and load if exists.
        """

        for candidate in CONFIG_FILE_CANDIDATES:
            if candidate and os.path.exists(candidate):
                return self._load_config(candidate)

        return {}

    def _load_config(self, config_file: str):
        """Read config file."""
        try:
            with open(config_file, "r") as ymlfile:
                return yaml.load(ymlfile, Loader=yaml.BaseLoader)
        except Exception as e:
            raise RuntimeError(f"Unable to load config file {e}") from None

    def get(self, name: str, default: Optional[str]) -> str:
        """Get config value by name.

        * First try to find it in environment.
        * Then look in data from config file.
        """
        config_env_var_name = f"MOSR_CONFIG_{name.upper()}"

        if config_env_var_name in os.environ:
            return os.environ.get(config_env_var_name)

        if name in self._data:
            return self._data[name]

        if default is not None:
            return default

        raise RuntimeError(f"Unable to get config parameter: {name}")

    @property
    def db_file(self) -> str:
        """Path to database file."""
        return self.get("db_file", None)

    @property
    def slack_webhook(self) -> str:
        """Url of slack webhook."""
        return self.get("slack_webhook", None)

    @property
    def server_bind(self) -> str:
        """Server listen interface."""
        return self.get("server_bind", "0.0.0.0")

    @property
    def server_port(self) -> int:
        """Server listen port."""
        return int(self.get("server_port", "8080"))

    @property
    def develop(self) -> bool:
        """enable developement helpers."""
        return self.get("develop", "False") != "False"

    @property
    def server_pass(self) -> str:
        """Server password."""
        return self.get("server_pass", None)


config = _Config()
"""_Config: Preloaded configuration object."""

import unittest  # pylint: disable=wrong-import-position
from unittest.mock import patch  # pylint: disable=wrong-import-position


class _ConfigTest(unittest.TestCase):
    #  pylint: disable=protected-access
    @patch.dict(os.environ, {"MOSR_CONFIG_TEST_CONFIG": "test_value_env"}, clear=True)
    def test_get_from_env(self):
        config = _Config()
        config._data = {"test_config": "test_value_data"}
        self.assertEqual("test_value_env", config.get("test_config", None))

    def test_get_from_data(self):
        config = _Config()
        config._data = {"test_config": "test_value_data"}
        self.assertEqual("test_value_data", config.get("test_config", None))

    def test_get_default(self):
        config = _Config()
        self.assertEqual("default_value", config.get("test_config", "default_value"))
